import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Config} from '../../models/config';

@Injectable()
export class QuestService {

  constructor( private http: HttpClient,
               private config: Config) {}

  sendAnswer(id: string, answer: string) {
      const httpOptions = {
          headers: new HttpHeaders({
              'Content-Type':  'application/json'
          })
      };
    return this.http.post<{result: string, data?: string, reason?: string}>(
        this.config.apiHost + 'answer',
        {id: id, answer: answer}, httpOptions);
  }
}
