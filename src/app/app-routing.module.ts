import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RunesCombinatorComponent} from './runes-combinator/runes-combinator.component';
import {QuestSolutionComponent} from './quest-solution/quest-solution.component';
import {GuideComponent} from "./guide/guide.component";

const routes: Routes = [
    {path: '', redirectTo: '/runes', pathMatch: 'full' },
    {path: 'runes', component: RunesCombinatorComponent},
    {path: 'quests', component: QuestSolutionComponent},
    {path: 'guide', component: GuideComponent},

];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
