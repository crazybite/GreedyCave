import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunesCombinatorComponent } from './runes-combinator.component';

describe('RunesCombinatorComponent', () => {
  let component: RunesCombinatorComponent;
  let fixture: ComponentFixture<RunesCombinatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunesCombinatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunesCombinatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
