import { Component } from '@angular/core';

@Component({
  selector: 'app-runes-combinator',
  templateUrl: './runes-combinator.component.html',
  styleUrls: ['./runes-combinator.component.css']
})
export class RunesCombinatorComponent {

    public combinationArray = {
        'ATK 100': [[7, 5, 4], '+100 к АТК'],
        'ATK 300': [[16, 7, 5], '+300 к АТК'],
        'ATK 600': [[18, 16, 7], '+600 к АТК'],
        'ATK 900': [[29, 18, 16], '+900 к АТК'],
        'ATK 1800': [[31, 29, 18], '+1800 к АТК'],

        'RUNE ATK 30': [[8, 4, 1], '+30% к АТК рун'],
        'RUNE ATK 60': [[15, 8, 4], '+60% к АТК рун'],
        'RUNE ATK 120': [[19, 15, 8], '+120% к АТК рун'],
        'RUNE ATK 180': [[28, 19, 15], '+180% к АТК рун'],
        'RUNE ATK 240': [[32, 28, 19], '+240% к АТК рун'],

        'DEF 1200': [ [31, 27, 25], '+1200 к ЗАЩ'],
        'DEF 600': [ [27, 25, 18], '+600 к ЗАЩ'],
        'DEF 400': [ [25, 18, 14], '+400 к ЗАЩ'],
        'DEF 200': [ [18, 14, 3], '+200 к ЗАЩ'],
        'DEF 100': [[14, 3, 1], '+100 к ЗАЩ'],

        'RUNE DEF 30': [[12, 9, 3], '+30% к ЗАЩ рун'],
        'RUNE DEF 60': [[14, 12, 9], '+60% к ЗАЩ рун'],
        'RUNE DEF 100': [[25, 14, 12], '+100% к ЗАЩ рун'],
        'RUNE DEF 150': [[27, 25, 14], '+150% к ЗАЩ рун'],
        'RUNE DEF 200': [[33, 27, 25], '+200% к ЗАЩ рун'],

        'HP 5500': [[30, 25, 22], '+5500 к ОЗ'],
        'HP 3500': [[25, 22, 14], '+3500 к ОЗ'],
        'HP 2000': [[22, 14, 12], '+2000 к ОЗ'],
        'HP 1000': [[14, 12, 1], '+1000 к ОЗ'],
        'HP 500': [[12, 9, 1], '+500 к ОЗ'],

        'RUNE HP 30': [[9, 3, 1], '+30% к ОЗ рун'],
        'RUNE HP 70': [[12, 6, 1], '+70% к ОЗ рун'],
        'RUNE HP 120': [[22, 12, 1], '+120% к ОЗ рун'],
        'RUNE HP 170': [[25, 22, 12], '+170% к ОЗ рун'],
        'RUNE HP 220': [[33, 25, 12], '+220% к ОЗ рун'],

        'MATK 50': [[8, 6, 2], '+50 к MATK'],
        'MATK 100': [[13, 10, 6], '+100 к MATK'],
        'MATK 200': [[17, 13, 10], '+200 к MATK'],
        'MATK 400': [[23, 17, 13], '+400 к MATK'],
        'MATK 650': [[30, 23, 17], '+650 к MATK'],

        'RUNE MATK 40': [[10, 6, 2], '+40% к MATK рун'],
        'RUNE MATK 100': [[17, 10, 6], '+100% к MATK рун'],
        'RUNE MATK 160': [[23, 17, 6], '+160% к MATK рун'],
        'RUNE MATK 220': [[26, 23, 17], '+220% к MATK рун'],
        'RUNE MATK 280': [[30, 26, 23], '+280% к MATK рун'],

        'MP 100': [[9, 6, 2], '+100 к ОМ'],
        'MP 200': [[13, 10, 2], '+200 к ОМ'],
        'MP 300': [[23, 13, 6], '+300 к ОМ'],
        'MP 500': [[26, 23 , 13], '+500 к ОМ'],
        'MP 900': [[34, 26, 13], '+900 к ОМ'],

        'RUNE MP 40': [[12, 6, 2], '+40% к ОМ рун'],
        'RUNE MP 100': [[13, 12, 6], '+100% к ОМ рун'],
        'RUNE MP 160': [[17, 13, 6], '+160% к ОМ рун'],
        'RUNE MP 220': [[26, 17, 13], '+220% к ОМ рун'],
        'RUNE MP 280': [[30, 26, 13], '+280% к ОМ рун'],

        'CRIT DMG 20': [ [11, 5, 4], '+20 к крит урону'],
        'CRIT DMG 40': [ [16, 15, 11], '+40 к крит урону'],
        'CRIT DMG 60': [ [24, 16, 15], '+60 к крит урону'],
        'CRIT DMG 80': [ [29, 24, 15], '+80 к крит урону'],
        'CRIT DMG 100': [[35, 29, 15], '+100 к крит урону'],

        'CRIT RATE 2': [[11, 8, 7], '+2 к вероятности крита'],
        'CRIT RATE 5': [[19, 18, 11], '+5 к вероятности крита'],
        'CRIT RATE 10': [[24, 19, 18], '+10 к вероятности крита'],
        'CRIT RATE 15': [[31, 24, 19], '+15 к вероятности крита'],
        'CRIT RATE 20': [[32, 31, 24], '+20 к вероятности крита'],

        'PRIERCE 100': [[8, 7, 4], '+100 к пробитию'],
        'PRIERCE 200': [[18, 8, 7], '+200 к пробитию'],
        'PRIERCE 300': [[19, 18, 8], '+300 к пробитию'],
        'PRIERCE 500': [[31, 19, 8], '+500 к пробитию'],
        'PRIERCE 1000': [[32, 31, 19], '+1000 к пробитию'],

        'RUNE PRIERCE 40': [[8, 7, 5], '+40% к пробитию рун'],
        'RUNE PRIERCE 80': [[18, 15, 8], '+80% к пробитию рун'],
        'RUNE PRIERCE 140': [[19, 18, 15], '+140% к пробитию рун'],
        'RUNE PRIERCE 200': [[28, 19, 18], '+200% к пробитию рун'],
        'RUNE PRIERCE 260': [[32, 28, 18], '+260% к пробитию рун'],

        'CHARM 10': [[11, 10, 2], '+10 к удаче'],
        'CHARM 20': [[23, 11, 10], '+20 к удаче'],
        'CHARM 30': [[24, 23, 11], '+30 к удаче'],
        'CHARM 40': [[34, 24, 11], '+40 к удаче'],
        'CHARM 50': [[35, 34, 24], '+50 к удаче'],

        'RUNE CHARM 20': [[11, 9, 1], '+20% к удаче рун'],
        'RUNE CHARM 40': [[22, 11, 9], '+40% к удаче рун'],
        'RUNE CHARM 60': [[24, 22, 11], '+60% к удаче рун'],
        'RUNE CHARM 80': [[33, 24, 11], '+80% к удаче рун'],
        'RUNE CHARM 100': [[35, 33, 24], '+100% к удаче рун'],

        'HP REGEN 20': [[9, 7, 1], '+20 к реген ОЗ'],
        'HP REGEN 40': [[12, 9, 7], '+40 к реген ОЗ'],
        'HP REGEN 80': [[22, 12, 9], '+80 к реген ОЗ'],
        'HP REGEN 150': [[25, 22, 17], '+150 к реген ОЗ'],
        'HP REGEN 290': [[33, 25, 22], '+290 к реген ОЗ'],

        'RUNE HP REGEN 50': [[11, 9, 1], '+50 к реген ОЗ рун'],
        'RUNE HP REGEN 100': [[22, 11, 9], '+100 к реген ОЗ рун'],
        'RUNE HP REGEN 160': [[24, 22, 11], '+160 к реген ОЗ рун'],
        'RUNE HP REGEN 220': [[33, 24, 11], '+220 к реген ОЗ рун'],
        'RUNE HP REGEN 280': [[35, 33, 24], '+280 к реген ОЗ рун'],

        'MP REGEN 15': [[10, 7, 2], '+15 к реген ОМ'],
        'MP REGEN 30': [[13, 10, 7], '+30 к реген ОМ'],
        'MP REGEN 50': [[23, 13, 10], '+50 к реген ОМ'],
        'MP REGEN 90': [[26, 23, 10], '+90 к реген ОМ'],
        'MP REGEN 180': [[34, 26, 23], '+180 к реген ОМ'],

        'RUNE MP REGEN 60': [[10, 6, 3], '+60 к реген ОМ рун'],
        'RUNE MP REGEN 120': [[17, 10, 2], '+120 к реген ОМ рун'],
        'RUNE MP REGEN 180': [[23, 17, 10], '+180 к реген ОМ рун'],
        'RUNE MP REGEN 240': [[30, 23, 10], '+240 к реген ОМ рун'],
        'RUNE MP REGEN 300': [[34, 30, 23], '300 к реген ОМ рун'],

        'RUNE GOLD 20': [[20, 7, 1], '+20% к золоту рун'],
        'RUNE GOLD 40': [[12, 20, 7], '+40% к золоту рун'],
        'RUNE GOLD 60': [[18, 12, 20], '+60% к золоту рун'],
        'RUNE GOLD 80': [[25, 18, 20], '+80% к золоту рун'],
        'RUNE GOLD 100': [[31, 25, 20], '+100% к золоту рун'],

        'RUNE EXP 20': [[21, 7, 3], '+20% к опыту рун'],
        'RUNE EXP 40': [[14, 21, 7], '+40% к опыту рун'],
        'RUNE EXP 60': [[18, 14, 21], '+60% к опыту рун'],
        'RUNE EXP 80': [[27, 18, 21], '+80% к опыту рун'],
        'RUNE EXP 100': [[31, 25, 20], '+100% к опыту рун']
    };

    public runesNames = {
        1: ['Айер', '+19 ОЗ', 'assets/runes/rune_1.png'],
        2: ['Айд', '+12 ОМ', 'assets/runes/rune_2.png'],
        3: ['Энни', '+16 к ЗАЩ', 'assets/runes/rune_3.png'],
        4: ['Назар', '+51 к мин. АТК', 'assets/runes/rune_4.png'],
        5: ['Эйс', '+116 к макс. АТК', 'assets/runes/rune_5.png'],
        6: ['Ис', '+23 к МАТК', 'assets/runes/rune_6.png'],
        7: ['Таль', '+70 к АТК', 'assets/runes/rune_7.png'],
        8: ['Ланна', '+55 к пробитию', 'assets/runes/rune_8.png'],
        9: ['Отт', '+12 к реген. ОЗ', 'assets/runes/rune_9.png'],
        10: ['Ширли', '+4 к реген. ОМ', 'assets/runes/rune_10.png'],
        11: ['Анна', '+5 к удаче', 'assets/runes/rune_11.png'],
        12: ['Сол', '+674 ОЗ', 'assets/runes/rune_12.png'],
        13: ['Салли', '+162 ОМ', 'assets/runes/rune_13.png'],
        14: ['Донна', '+193 ЗАЩ', 'assets/runes/rune_14.png'],
        15: ['Гарри', '+565 к мин. АТК', 'assets/runes/rune_15.png'],
        16: ['Ало', '+1312 к макс. АТК', 'assets/runes/rune_16.png'],
        17: ['Лум', '+216 к МАТК', 'assets/runes/rune_17.png'],
        18: ['Кен', '+580 к АТК', 'assets/runes/rune_18.png'],
        19: ['Вал', '+396 к пробитию', 'assets/runes/rune_19.png'],
        20: ['Лэмми', '+10 доп. золота', 'assets/runes/rune_20.png'],
        21: ['Уми', '+10 доп. опыта', 'assets/runes/rune_21.png'],
        22: ['Квазар', '+94 к реген. ОЗ', 'assets/runes/rune_22.png'],
        23: ['Марр', '+26 к реген. ОМ', 'assets/runes/rune_23.png'],
        24: ['Истед', '+25 к удаче', 'assets/runes/rune_24.png'],
        25: ['Гхор', '+3884 ОЗ', 'assets/runes/rune_25.png'],
        26: ['Фокси', '+857 ОМ', 'assets/runes/rune_26.png'],
        27: ['Омс', '+942 к ЗАЩ', 'assets/runes/rune_27.png'],
        28: ['Роман', '+2583 к мин. АТК', 'assets/runes/rune_28.png'],
        29: ['Садам', '+5645 к макс. АТК', 'assets/runes/rune_29.png'],
        30: ['Бэй', '+879 к МАТК', 'assets/runes/rune_30.png'],
        31: ['Джо', '+2229 к АТК', 'assets/runes/rune_31.png'],
        32: ['Кудар', '+1451 к пробитию', 'assets/runes/rune_32.png'],
        33: ['Сэйд', '+262 к реген. ОЗ', 'assets/runes/rune_33.png'],
        34: ['Сэмми', '+97 к реген. ОМ', 'assets/runes/rune_34.png'],
        35: ['Зелл', '+100 к удаче', 'assets/runes/rune_35.png']
    };
    public runesKeys;
    public result;
    public mbresult;
    public smithSay = '';
    constructor() {
      this.runesKeys = Object.keys(this.runesNames);
      this.smithSaying('Здравствуй путник!<br>\n' +
        '       Укажи мне руны, которые у тебя есть в наличии,' +
        '       а я тебе расскажу про возможные комбинации с ними!<br>\n' +
        '       Ты можешь указать путем клика на руны справа или просто ввести в поле ввода по центру номера рун через зяаптую.');
    }

    getCombinattions(string) {
        this.result = [];
        this.mbresult = [];
        const availableRunes = [];
        const runes = string.split(',');
        for (const rune of runes) {
            availableRunes.push(parseInt(rune.trim(), 10));
        }

        let found = [];
        for (const combination in this.combinationArray) {
            if (this.combinationArray.hasOwnProperty(combination)) {
                found = [];
                for (const rune in this.combinationArray[combination][0]) {
                    if (this.combinationArray[combination][0].hasOwnProperty(rune)) {
                        if (availableRunes.indexOf(this.combinationArray[combination][0][rune]) !== -1) {
                            found.push(this.combinationArray[combination][0][rune]);
                        }
                    }
                }
                if (found.length === 3) {
                    this.result.push({name: combination, runes: this.combinationArray[combination]});
                }
                if (found.length === 2) {
                    this.mbresult.push({name: combination, runes: this.combinationArray[combination], found: found});
                }
            }
        }
        let smithSay = '';
        if (this.result.length === 0) {
          smithSay = 'Плохие новости, я не вижу возможных комбинаций, тебе нужно раздобыть еще рун. <br>';
          if (this.mbresult.length > 0) {
              smithSay += 'А теперь хорошие новсти! Есть ' + this.mbresult.length + ' ' +
                  this.declOfNum(this.mbresult.length, ['комбинация', 'комбинации', 'комбинаций']) +
                  ' , где нехватает одной руны.<br>';
          }
          smithSay += 'Объединив три одинаковые руны, ты можешь получить одну руну следующего уровня!';
        } else {
            smithSay = 'У меня хорошие новости! Из твоих рун может получиться ' + this.result.length + ' ' +
                this.declOfNum(this.result.length, ['комбинация', 'комбинации', 'комбинаций']) + ' !<br>';
            if (this.mbresult.length > 0) {
              smithSay += 'А еще ' + this.mbresult.length + ' ' + this.declOfNum(this.mbresult.length,
                  ['комбинация', 'комбинации', 'комбинаций']) + ' , где нехватает одной руны.<br>';
          }
        }
        this.smithSaying(smithSay);
    }
    declOfNum(number, titles) {
        const cases = [2, 0, 1, 1, 1, 2];
        return titles[ (number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5] ];
    }
    onKey(event, text) {
      if (event.keyCode === 13) {
          this.getCombinattions(text);
      }
    }
    smithSaying(text) {
      const words = ['Хм..', 'Эх..', 'Кхм..', 'Апчхи!', ' Пффф..', 'Ой..', 'Блин..'];
      this.smithSay = words[Math.floor(Math.random() * (words.length - 1))];
      setTimeout(() => {this.smithSay = text; } , 1000);
    }
}
