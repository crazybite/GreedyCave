import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { RunesCombinatorComponent } from './runes-combinator/runes-combinator.component';
import { NavComponent } from './nav/nav.component';
import { AppRoutingModule} from './app-routing.module';
import { QuestSolutionComponent } from './quest-solution/quest-solution.component';
import {HttpClient} from '@angular/common/http';

import {QuestService} from './service/quest/quest.service';
import {Config} from './models/config';
import { GuideComponent } from './guide/guide.component';

@NgModule({
  declarations: [
    AppComponent,
    RunesCombinatorComponent,
    NavComponent,
    QuestSolutionComponent,
    GuideComponent
  ],
  imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule
  ],
  providers: [
      QuestService,
      HttpClient,
      Config
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
