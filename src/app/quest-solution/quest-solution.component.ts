import {Component } from '@angular/core';
import {QuestService} from '../service/quest/quest.service';
import * as jsondata from './quests.json';

@Component({
  selector: 'app-quest-solution',
  templateUrl: './quest-solution.component.html',
  styleUrls: ['./quest-solution.component.css']
})
export class QuestSolutionComponent {
    public infoMessage = '';
  public questionBase = <any>jsondata;
  public resultQuestions = [];
  public smithSay = '';
  public answersHistory = [];

  constructor(private questionService: QuestService) {
      this.smithSaying('Тебя интересуют ответы на задания в пещерах?<br>' +
      ' Просто дай мне знать что тебя спросили и я постараюсь тебе помочь.' +
      ' <b>Сорян, вчера я был на вечеринке в башне ну и.... вообщем ' +
      ' забыл некоторые ответы... постараюсь вспомнить попозже..</b>' +
      ' Если у тебя есть время помоги мне, просто укажи правильный ответ. Буду очень благодарен!');
  }

  getQuests(text: string) {
      if (text.length <= 3) {
        this.smithSaying('Мне бы побольше информации..., раскажи мне больше!');
        return false;
      }
      this.resultQuestions = [];
      for (const question of this.questionBase) {
          if (question['question'].toLowerCase().indexOf(text.toLowerCase()) !== -1) {
              this.resultQuestions.push(question);
          }
      }
      if (this.resultQuestions.length > 0) {
          this.smithSaying( 'Хм... Да! Я встречал этот вопрос!');
      } else {
          this.smithSaying('Неее, таких вопросов я еще не встречал... Может ты где то ошибся?' +
              '<a href="mailto:web@greedycave.xyz">' +
              'Если все врено, то напиши мне на почту, или скинь скрин</a> и в следующий раз он появится.');
      }
  }
  onKey(event, text) {
      if (event.keyCode === 13) {
          this.getQuests(text);
      }
  }

  addAnswer(event, question, answer) {
      if (this.answersHistory.indexOf(question.id) !== -1) {
          this.smithSaying('Эй! да ты же уже жамкал на этот вопрос!');
          return false;
      }
      if (question.correct === '') {
          this.questionService.sendAnswer(question.id, answer).subscribe( data => {
              if (data['status'] === 'OK') {
                  this.smithSaying('Спасибо! Ваши данные учтены!');
                  question.correct = answer;
              } else if (data['status'] === 'error') {
                  if (data.hasOwnProperty('reason')) {
                      switch (data['reason']) {
                          case 'doubled':
                              this.smithSaying('Если мне не изменяет память, ты уже отвечал на этот вопрос...<br>' +
                                  'Я анализирую ваши данные, спасибо!');
                              break;
                          default:
                              this.smithSaying('Упсс.. что то пошло не так. Я сейчас свяжусь с админом, попрошу что бы все сиправил.');
                              break;
                      }
                  } else {
                      this.smithSaying('Упсс.. что то пошло не так. Я сейчас свяжусь с админом, попрошу что бы все сиправил.');
                  }
              }
          }, error => {
              this.smithSaying('Произошла ошибка! Ваши данные не учтены ... Скоро мы все починим.');
          });
      }
      this.answersHistory.push(question.id);
  }
  smithSaying(text) {
      const words = ['Хм..', 'Эх..', 'Кхм..', 'Апчхи!', ' Пффф..', 'Ой..', 'Блин..'];
      this.smithSay = words[Math.floor(Math.random() * (words.length - 1))];
      setTimeout(() => {this.smithSay = text; } , 1000);
  }
}
