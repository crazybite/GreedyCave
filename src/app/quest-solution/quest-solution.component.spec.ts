import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestSolutionComponent } from './quest-solution.component';

describe('QuestSolutionComponent', () => {
  let component: QuestSolutionComponent;
  let fixture: ComponentFixture<QuestSolutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestSolutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestSolutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
